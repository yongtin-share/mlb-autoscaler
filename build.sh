#!/bin/bash

REPO="yongtin/marathon-lb-autoscaler"
docker build --no-cache=true -t=${REPO}:latest .
FORWARD_TAG=`docker images -f "dangling=false" ${REPO} | grep 'latest' | awk '{print $3}'`
docker tag ${FORWARD_TAG} ${REPO}:latest

docker push yongtin/marathon-lb-autoscaler:latest
