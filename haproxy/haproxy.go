package haproxy

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

type HAproxy struct {
	URL           string
	StatsEndPoint string
}

func Setup(url string) (h HAproxy) {
	h.URL = url
	h.StatsEndPoint = "/haproxy?stats;csv"
	return h
}

func (h *HAproxy) GetAppStatus(appName string) (sessionRateOverall int, err error) {
	allDat, err := h.pollStats()

	// search for the appName's backend to get overall current traffic
	for _, dat := range allDat {
		if dat[0] == appName && dat[1] == "BACKEND" {
			// in the csv, field 34 (dat[33]) is the 'rate' field
			return strconv.Atoi(dat[33])
		}
	}

	return 0, fmt.Errorf("match_error: '%s BACKEND not found'", appName)
}

func (h *HAproxy) pollStats() (dat [][]string, err error) {
	u, err := url.Parse(h.URL + h.StatsEndPoint)
	if err != nil {
		return dat, err
	}
	client := &http.Client{}
	var res *http.Response
	if req, err := http.NewRequest("GET", u.String(), nil); err != nil {
		return dat, err
	} else if res, err = client.Do(req); err != nil {
		return dat, err
	}
	defer res.Body.Close()

	r := csv.NewReader(res.Body)
	return r.ReadAll()
}
