package haproxy

import "testing"
import "fmt"
import "github.com/stretchr/testify/assert"
import log "github.com/inconshreveable/log15"
import "os"

func TestHAProxy(t *testing.T) {

	assert.NotEmpty(t, os.Getenv("HAPROXY_URL"), "HAPROXY_URL not set")

	lb := Setup(os.Getenv("HAPROXY_URL"))
	dat, err := lb.pollStats()

	log.Info(fmt.Sprintf("%s is dat[4]", dat[4]))
	assert.Nil(t, err)
}

func TestPollAppStatus(t *testing.T) {
	assert.NotEmpty(t, os.Getenv("HAPROXY_URL"), "HAPROXY_URL not set")

	lb := Setup(os.Getenv("HAPROXY_URL"))
	currentTraffic, err := lb.GetAppStatus("tyong_app-hello-go_12222")

	log.Info(fmt.Sprintf("currentTraffic: %d", currentTraffic))
	assert.Nil(t, err)
}
