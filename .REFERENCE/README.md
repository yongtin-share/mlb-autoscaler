# marathon-lb-autoscale

TODO: 

![Incredible diagram](https://raw.github.com/mesosphere/marathon-lb-autoscale/master/marathon-lb-autoscale.png)

## How do I use it?

```
NAME:
   marathon-lb-autoscaler - Autoscaling controller backed by marathon-lb (haproxy)

USAGE:
   controller [global options] command [command options] [arguments...]

VERSION:
   0.0.1

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --marathon value, -m value             URL for Marathon (default: "http://leader.mesos:8080") [$MARATHON_URL]
   --marathon_app_name value, -n value    Marathon AppID [$MARATHON_APPID]
   --haproxy value, -p value              HAProxy URL (default: "http://marathon-lb.marathon.mesos:9090") [$HAPROXY_URL]
   --haproxy_app_name value, -a value     haproxy app name [$HAPROXY_APP_NAME]
   --threshold_instances value, -s value  Scaling will occur when the target number of instances differs from the actual number by at least this amount (default: 3) [$THRESHOLD_INSTANCES]
   --target_rate value, -t value          Target number of requests per second per app instance (default: 100) [$TARGET_RATE]
   --cooldown value, -c value             Number of additional seconds to wait after making a scale change (default: 300) [$COOLDOWN_PERIOD]
   --interval value, -i value             Number of seconds (N) between update intervals (default: 10) [$INTERVAL]
   --max value                            Maximum number of instances an app may be scaled to (default: 10000) [$MAX_INSTANCES]
   --min value                            Number of seconds (N) between update intervals (default: 1) [$MIN_INSTANCES]
   --help, -h                             show help
   --version, -v                          print the version
```

## Running on Marathon

```json
{
  "id": "marathon-lb-autoscale",
  "args":[
    "--marathon", "http://leader.mesos:8080",
    "--haproxy", "http://marathon-lb.marathon.mesos:9090",
    "--apps", "nginx_10000"
  ],
  "cpus": 0.1,
  "mem": 16.0,
  "instances": 1,
  "container": {
    "type": "DOCKER",
    "docker": {
      "image": "mesosphere/marathon-lb-autoscale",
      "network": "HOST",
      "forcePullImage": true
    }
  }
}
```
