package scaler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type MarathonScaler struct {
	URL          string
	AppsEndPoint string
}

// New gets a new marathon scaler
func New(url string) (m MarathonScaler, e error) {
	m.URL = url
	m.AppsEndPoint = "/v2/apps/"
	return m, nil
}

// Scale supports scaling up and down instances
func (m *MarathonScaler) Scale(appName string, newScale int) (msg string, err error) {
	u, err := url.Parse(m.URL + m.AppsEndPoint + appName)
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	var res *http.Response

	jsonRawRequest := []byte(fmt.Sprintf(`{"instances": %d}`, newScale))
	jsonRequest := bytes.NewBuffer(jsonRawRequest)
	req, err := http.NewRequest("PUT", u.String(), jsonRequest)
	if err != nil {
		return "", err
	}
	req.Header.Add("Content-Type", "application/json")

	res, err = client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	var dat []byte
	dat, err = ioutil.ReadAll(res.Body)

	// valid marathon responses are: 200, 400, 401, 403, 404, 409, 422
	if res.StatusCode == 200 {
		return string(dat), nil
	} else {
		return fmt.Sprintf(`{"code": %d, "err_message": %s}`, res.StatusCode, string(dat)), fmt.Errorf("ScaleUp responde %d with message %s", res.StatusCode, string(dat))
	}
}

// GetScale gets the scale (num of instances) of an app
func (m *MarathonScaler) GetScale(appName string) (scale int, err error) {
	u, err := url.Parse(m.URL + m.AppsEndPoint + appName)
	if err != nil {
		return scale, err
	}

	client := &http.Client{}
	var res *http.Response

	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return scale, err
	}
	req.Header.Add("Content-Type", "application/json")

	res, err = client.Do(req)
	if err != nil {
		return scale, err
	}
	defer res.Body.Close()

	var jsonDat map[string]interface{}
	var dat []byte
	dat, err = ioutil.ReadAll(res.Body)
	err = json.Unmarshal(dat, &jsonDat)
	if err != nil {
		return scale, err
	}
	//fmt.Println(string(dat))

	scale = int(jsonDat["app"].(map[string]interface{})["instances"].(float64))
	return scale, nil
}
