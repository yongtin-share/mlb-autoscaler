package scaler

import (
	"fmt"
	"os"
	"testing"

	log "github.com/inconshreveable/log15"
	"github.com/stretchr/testify/assert"
)

func TestScaleUp(t *testing.T) {
	// marathon_url := "http://leader.mesos:8080"

	assert.NotEmpty(t, os.Getenv("MARATHON_URL"), "MARATHON_URL not set")

	var marathonURL = os.Getenv("MARATHON_URL")

	m, err := New(marathonURL)
	assert.Nil(t, err)
	scale, err := m.GetScale("/tyong/app-hello-go")
	log.Info(fmt.Sprintf("scale = %d", scale))

	newScale := scale + 3
	msg, err := m.Scale("/tyong/app-hello-go", newScale)
	log.Info(fmt.Sprintf("msg = %s", msg))
	assert.Nil(t, err)

	scale, err = m.GetScale("/tyong/app-hello-go")
	log.Info(fmt.Sprintf("scale = %d", scale))
}
