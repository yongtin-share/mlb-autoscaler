package main

import (
	"fmt"
	"math"
	"os"
	"time"

	log "github.com/inconshreveable/log15"
	"github.com/urfave/cli"
	"gitlab.com/yongtin/mlb-autoscaler.git/haproxy"
	"gitlab.com/yongtin/mlb-autoscaler.git/scaler"
)

type Options struct {
	MarathonURL        string
	MarathonAppID      string
	HAProxy            string
	HAProxyAppName     string
	ThresholdInstances int
	TargetRate         int
	CoolDown           int
	Interval           int
	Max                int
	Min                int
}

func main() {
	app := cli.NewApp()
	app.Name = "marathon-lb-autoscaler"
	app.Usage = "Autoscaling controller backed by marathon-lb (haproxy)"
	app.Version = "0.0.1"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "marathon, m",
			Value:  "http://leader.mesos:8080",
			Usage:  "URL for Marathon",
			EnvVar: "MARATHON_URL",
		},
		cli.StringFlag{
			Name:   "marathon_app_name, n",
			Value:  "",
			Usage:  "Marathon AppID",
			EnvVar: "MARATHON_APPID",
		},
		cli.StringFlag{
			Name:   "haproxy, p",
			Value:  "http://marathon-lb.marathon.mesos:9090",
			Usage:  "HAProxy URL",
			EnvVar: "HAPROXY_URL",
		},
		cli.StringFlag{
			Name:   "haproxy_app_name, a",
			Value:  "",
			Usage:  "haproxy app name",
			EnvVar: "HAPROXY_APP_NAME",
		},
		cli.IntFlag{
			Name:   "threshold_instances, s",
			Value:  3,
			Usage:  "Scaling will occur when the target number of instances differs from the actual number by at least this amount",
			EnvVar: "THRESHOLD_INSTANCES",
		},
		cli.IntFlag{
			Name:   "target_rate, t",
			Value:  100,
			Usage:  "Target number of requests per second per app instance",
			EnvVar: "TARGET_RATE",
		},
		cli.IntFlag{
			Name:   "cooldown, c",
			Value:  300,
			Usage:  "Number of additional seconds to wait after making a scale change",
			EnvVar: "COOLDOWN_PERIOD",
		},
		cli.IntFlag{
			Name:   "interval, i",
			Value:  10,
			Usage:  "Number of seconds (N) between update intervals",
			EnvVar: "INTERVAL",
		},
		cli.IntFlag{
			Name:   "max",
			Value:  10000,
			Usage:  "Maximum number of instances an app may be scaled to",
			EnvVar: "MAX_INSTANCES",
		},
		cli.IntFlag{
			Name:   "min",
			Value:  1,
			Usage:  "Number of seconds (N) between update intervals",
			EnvVar: "MIN_INSTANCES",
		},
	}
	app.Action = func(c *cli.Context) error {
		if c.String("marathon_app_name") == "" {
			return fmt.Errorf("Missing required parameter: marathon_app_name")
		}
		if c.String("haproxy_app_name") == "" {
			return fmt.Errorf("Missing required parameter: haproxy_app_name")
		}

		return runController(Options{
			MarathonURL:        c.String("marathon"),
			MarathonAppID:      c.String("marathon_app_name"),
			HAProxy:            c.String("haproxy"),
			HAProxyAppName:     c.String("haproxy_app_name"),
			ThresholdInstances: c.Int("threshold_instances"),
			TargetRate:         c.Int("target_rate"),
			CoolDown:           c.Int("cooldown"),
			Interval:           c.Int("interval"),
			Max:                c.Int("max"),
			Min:                c.Int("min"),
		})
	}

	app.Run(os.Args)

}

func runController(o Options) error {
	var targetSessionRatePerNode = o.TargetRate
	var marathonURL = o.MarathonURL
	var appName = o.MarathonAppID
	var haproxyEndPoint = o.HAProxy
	var haproxyAppName = o.HAProxyAppName
	var instanceThreshold = o.ThresholdInstances
	var coolDownPeriod = o.CoolDown
	var pollInterval = o.Interval

	var marathon scaler.MarathonScaler
	var err error
	var currentInstances int
	var lastScaleTracker = coolDownPeriod

	marathon, err = scaler.New(marathonURL)
	if err != nil {
		log.Error(err.Error())
	}
	for {
		time.Sleep(time.Second * time.Duration(pollInterval))

		// get current number of instances
		currentInstances, err = marathon.GetScale(appName)
		if err != nil {
			log.Error(fmt.Sprintf("%v", err))
			continue
		}

		// calculateTarget (based on appname, targetsessionrate)
		var targetInstances int
		var currentSessionRate int
		targetInstances, currentSessionRate, err = calculateTargetInstances(haproxyEndPoint, haproxyAppName, targetSessionRatePerNode)
		if err != nil {
			log.Error(fmt.Sprintf("%v", err))
			continue
		}

		// scale if outside last cooldown period
		if outsideCoolDown(lastScaleTracker, coolDownPeriod) {
			if targetInstances > instanceThreshold+currentInstances {
				var msg string
				log.Info(fmt.Sprintf("scale up: app=%s,haproxy_app=%s,current=%d,target=%d,overall_current_rate=%d,target_rate=%d", appName, haproxyAppName, currentInstances, targetInstances, currentSessionRate, targetSessionRatePerNode))
				msg, err = marathon.Scale(appName, targetInstances)
				lastScaleTracker = 0
				if err != nil {
					log.Error(fmt.Sprintf("%v", err))
				} else {
					log.Info(fmt.Sprintf("%s", msg))
				}
			} else if targetInstances < currentInstances-instanceThreshold {
				log.Info(fmt.Sprintf("scale down: app=%s,haproxy_app=%s,current=%d,target=%d,overall_current_rate=%d,target_rate=%d", appName, haproxyAppName, currentInstances, targetInstances, currentSessionRate, targetSessionRatePerNode))
				var msg string
				msg, err = marathon.Scale(appName, targetInstances)
				lastScaleTracker = 0
				if err != nil {
					log.Error(fmt.Sprintf("%v", err))
				} else {
					log.Info(fmt.Sprintf("%s", msg))
				}
			} else {
				log.Info(fmt.Sprintf("no scaling need: app=%s,haproxy_app=%s,current=%d,target=%d,overall_current_rate=%d,target_rate=%d", appName, haproxyAppName, currentInstances, targetInstances, currentSessionRate, targetSessionRatePerNode))
			}
		} else {
			log.Info(fmt.Sprintf("cooling down skip scale: app=%s,haproxy_app=%s,current=%d,target=%d,overall_current_rate=%d,target_rate=%d,next_scaling=%ds", appName, haproxyAppName, currentInstances, targetInstances, currentSessionRate, targetSessionRatePerNode, coolDownPeriod-lastScaleTracker))
		}

		lastScaleTracker += pollInterval
	}

}

func outsideCoolDown(lastScaleTracker int, coolDownPeriod int) bool {
	return lastScaleTracker >= coolDownPeriod
}

func calculateTargetInstances(haproxyEndPoint string, appName string, targetSessionRate int) (target int, currentSessionRateOverall int, err error) {
	lb := haproxy.Setup(haproxyEndPoint)
	currentSessionRateOverall, err = lb.GetAppStatus(appName)
	if err != nil {
		return -1, -1, err
	}
	target = int(math.Ceil(float64(currentSessionRateOverall)/float64(targetSessionRate))) + 1
	return target, currentSessionRateOverall, err
}
