FROM golang

ADD . /go/src/gitlab.com/yongtin/mlb-autoscaler.git
RUN go get github.com/inconshreveable/log15
RUN go get github.com/stretchr/testify/assert
RUN go get github.com/urfave/cli
WORKDIR /go/src/gitlab.com/yongtin/mlb-autoscaler.git
RUN go build -o /go/bin/controller .

ENTRYPOINT [ "/go/bin/controller" ]
